This package contains an [`io-ts`](https://github.com/gcanti/io-ts)
[decoder](https://github.com/gcanti/io-ts/blob/master/Decoder.md)
combinator that turns a decoder expecting possibly nested objects into a
decoder expecting a flat object whose keys are styled in `UPPER_SNAKE_CASE`,
like how environment variables are defined.

This combinator will therefore let you compose an environment variable parser
from smaller object decoder units -- perhaps controlling different parts of your
application -- and taking care of converting names for you, as long as the
property names of your object decoder units consistently follow one of the
supported casing conventions.

```ts
import { forEnv } from "io-ts-env";
import { string } from "io-ts/lib/Decoder";
import { pipe } from "fp-ts/pipeable";

const decodeEnv = pipe(
  type({
    foo: string,
    barBaz: string,
    nested: type({
      innerProperty: string
    })
  }),
  forEnv()
);
```

The resulting decoder will first preproces and environment variable object
(for instance, `process.env`, or a parsed `.env` file using e.g.
[`envfile`](https://www.npmjs.com/package/envfile)) looking like this:

```ts
{
  FOO: "some value",
  BAR_BAZ: "another value",
  NESTED__INNER_PROPERTY: "nested value"
}
```

The resulting environment variable decoder will have convert names with words
from the original properties (inferred from casing conventions) separated by
`_` and nested object property names are concatnated separated by `__`. Both
separators can be [overridden](src/lib/EnvDecoderOptions.ts). For instance,
the combinator below will convert the variable `NESTED_INNER-PROPERTY=value`
into `{ Nested: { InnerProperty: "value" }}`

```ts
/**
 * This will now match environment variables like:
 * `NESTED_INNER-PROPERTY` and convert it into
 *
 */
forEnv({
  wordSeparator: "-",
  objectSeparator: "_",
  wordCasing: "upperCamelCase"
});
```

## Supported casing conventions

- `UpperCamelCase`
- `lowerCamelCase` (default)
- `UPPER-KEBAB-CASE`
- `lower-kebab-case`
- `UPPER_SNAKE_CASE`
- `lower_snake_case`

## Error reporting

The package attempts to map back key errors from io-ts into the environment
variable name, so that you won't have to remember the convention you set up.
So a property `fooBar` should appear as `FOO_BAR` in the error report.

However, there are limitations. By design of io-ts `Decoder` which swallows all
internal definitions and exposes only the decoder method, it is not possible
to get the generated full path of the environment variable that is missing in a
nested object, because the `type` decoder will bail out as soon as an outer key
is missing. So the variable `NESTED_INNER-PROPERTY` would first fail saying that
the key `NESTED` is missing, but once you have that, it would complain that
`NESTED_INNER-PROPERTY` is missing. This also means that it is not possible to
list all environment variables from the decoder, since the information of its
properties cannot be accessed.

Issues like these could be circumvented by using io-ts `Type` or `Codec`, where
property definitions are still accessible. A combinator for them may be
implemented in future.
