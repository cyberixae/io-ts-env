## [0.4.1](https://gitlab.com/soul-codes/io-ts-env/compare/0.4.0...0.4.1) (2020-10-31)


### Bug Fixes

* 🐛 filter out env vars that translate to no word/phrase ([0fd45a6](https://gitlab.com/soul-codes/io-ts-env/commit/0fd45a61b31a61e9587e1edafcafe74d84157ad7))

# 0.4.0 (2020-10-21)
