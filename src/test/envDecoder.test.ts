import { forEnv } from "@lib";
import { getOrElseW, mapLeft, right, swap } from "fp-ts/lib/Either";
import { pipe } from "fp-ts/lib/pipeable";
import { draw, string, type } from "io-ts/lib/Decoder";

test("default options", () => {
  const schema = pipe(
    type({
      foo: string,
      barBaz: string,
      somethingNiceAndLong: string,
      nested: type({
        floof: string,
        doubly: type({
          superNested: string,
        }),
      }),
    }),
    forEnv()
  );

  expect(
    schema.decode({
      FOO: "this is foo",
      BAR_BAZ: "this is barBaz",
      SOMETHING_NICE_AND_LONG: "this is something nice and long",
      NESTED__FLOOF: "this is nested floof",
      NESTED__DOUBLY__SUPER_NESTED: "this is soooo nested",
    })
  ).toEqual(
    right({
      foo: "this is foo",
      barBaz: "this is barBaz",
      somethingNiceAndLong: "this is something nice and long",
      nested: {
        floof: "this is nested floof",
        doubly: {
          superNested: "this is soooo nested",
        },
      },
    })
  );
});

test("changing word and object separators", () => {
  const schema = pipe(
    type({
      foo: string,
      barBaz: string,
      somethingNiceAndLong: string,
      nested: type({
        floof: string,
        doubly: type({
          superNested: string,
        }),
      }),
    }),
    forEnv({ wordSeparator: "-", objectSeparator: "_" })
  );

  expect(
    schema.decode({
      FOO: "this is foo",
      "BAR-BAZ": "this is barBaz",
      "SOMETHING-NICE-AND-LONG": "this is something nice and long",
      NESTED_FLOOF: "this is nested floof",
      "NESTED_DOUBLY_SUPER-NESTED": "this is soooo nested",
    })
  ).toEqual(
    right({
      foo: "this is foo",
      barBaz: "this is barBaz",
      somethingNiceAndLong: "this is something nice and long",
      nested: {
        floof: "this is nested floof",
        doubly: {
          superNested: "this is soooo nested",
        },
      },
    })
  );
});

test("one-word property", () => {
  const schema = pipe(
    type({
      foo: string,
    }),
    forEnv({ wordSeparator: "-", objectSeparator: "_" })
  );
  const actual = pipe(
    schema.decode({}),
    mapLeft(draw),
    swap,
    getOrElseW((): never => {
      throw Error("Expected the decoding to fail but it passed.");
    })
  );
  const expected = [
    `required property "FOO"`,
    `└─ cannot decode undefined, should be string`,
  ].join("\n");
  expect(actual).toBe(expected);
});

test("multiword property", () => {
  const schema = pipe(
    type({
      fooBar: string,
    }),
    forEnv({ wordSeparator: "-", objectSeparator: "_" })
  );
  const actual = pipe(
    schema.decode({}),
    mapLeft(draw),
    swap,
    getOrElseW((): never => {
      throw Error("Expected the decoding to fail but it passed.");
    })
  );
  const expected = [
    `required property "FOO-BAR"`,
    `└─ cannot decode undefined, should be string`,
  ].join("\n");
  expect(actual).toBe(expected);
});

test("nested property (error on inner property)", () => {
  const schema = pipe(
    type({
      fooBar: type({
        baz: type({
          qux: string,
        }),
      }),
    }),
    forEnv({ wordSeparator: "-", objectSeparator: "_" })
  );
  const actual = pipe(
    schema.decode({ "FOO-BAR_BAZ_QUX": void 0 }),
    mapLeft(draw),
    swap,
    getOrElseW((): never => {
      throw Error("Expected the decoding to fail but it passed.");
    })
  );
  const expected = [
    `required property "FOO-BAR_BAZ_QUX"`,
    `└─ cannot decode undefined, should be string`,
  ].join("\n");
  expect(actual).toBe(expected);
});
