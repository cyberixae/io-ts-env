import { WordCasingStyle, forEnv } from "@lib";
import { right } from "fp-ts/lib/Either";
import { pipe } from "fp-ts/lib/pipeable";
import { Decoder, type } from "io-ts/lib/Decoder";
import { string } from "io-ts/lib/Decoder";

describe("different casing styles", () => {
  const cases: Record<WordCasingStyle, [Decoder<unknown, unknown>, any]> = {
    lowerCamelCase: [
      type({
        foo: string,
        barBaz: string,
        somethingNiceAndLong: string,
        nested: type({
          floof: string,
          doubly: type({
            superNested: string,
          }),
        }),
      }),
      {
        foo: "this is foo",
        barBaz: "this is barBaz",
        somethingNiceAndLong: "this is something nice and long",
        nested: {
          floof: "this is nested floof",
          doubly: {
            superNested: "this is soooo nested",
          },
        },
      },
    ],
    upperCamelCase: [
      type({
        Foo: string,
        BarBaz: string,
        SomethingNiceAndLong: string,
        Nested: type({
          Floof: string,
          Doubly: type({
            SuperNested: string,
          }),
        }),
      }),
      {
        Foo: "this is foo",
        BarBaz: "this is barBaz",
        SomethingNiceAndLong: "this is something nice and long",
        Nested: {
          Floof: "this is nested floof",
          Doubly: {
            SuperNested: "this is soooo nested",
          },
        },
      },
    ],
    lowerKebabCase: [
      type({
        foo: string,
        "bar-baz": string,
        "something-nice-and-long": string,
        nested: type({
          floof: string,
          doubly: type({
            "super-nested": string,
          }),
        }),
      }),
      {
        foo: "this is foo",
        "bar-baz": "this is barBaz",
        "something-nice-and-long": "this is something nice and long",
        nested: {
          floof: "this is nested floof",
          doubly: {
            "super-nested": "this is soooo nested",
          },
        },
      },
    ],
    upperKebabCase: [
      type({
        FOO: string,
        "BAR-BAZ": string,
        "SOMETHING-NICE-AND-LONG": string,
        NESTED: type({
          FLOOF: string,
          DOUBLY: type({
            "SUPER-NESTED": string,
          }),
        }),
      }),
      {
        FOO: "this is foo",
        "BAR-BAZ": "this is barBaz",
        "SOMETHING-NICE-AND-LONG": "this is something nice and long",
        NESTED: {
          FLOOF: "this is nested floof",
          DOUBLY: {
            "SUPER-NESTED": "this is soooo nested",
          },
        },
      },
    ],
    lowerSnakeCase: [
      type({
        foo: string,
        bar_baz: string,
        something_nice_and_long: string,
        nested: type({
          floof: string,
          doubly: type({
            super_nested: string,
          }),
        }),
      }),
      {
        foo: "this is foo",
        bar_baz: "this is barBaz",
        something_nice_and_long: "this is something nice and long",
        nested: {
          floof: "this is nested floof",
          doubly: {
            super_nested: "this is soooo nested",
          },
        },
      },
    ],
    upperSnakeCase: [
      type({
        FOO: string,
        BAR_BAZ: string,
        SOMETHING_NICE_AND_LONG: string,
        NESTED: type({
          FLOOF: string,
          DOUBLY: type({
            SUPER_NESTED: string,
          }),
        }),
      }),
      {
        FOO: "this is foo",
        BAR_BAZ: "this is barBaz",
        SOMETHING_NICE_AND_LONG: "this is something nice and long",
        NESTED: {
          FLOOF: "this is nested floof",
          DOUBLY: {
            SUPER_NESTED: "this is soooo nested",
          },
        },
      },
    ],
  };

  (Object.keys(cases) as WordCasingStyle[]).forEach((wordCasing) =>
    test(wordCasing, () => {
      const [typeSchema, decodedValue] = cases[wordCasing];
      const schema = pipe(typeSchema, forEnv({ wordCasing: wordCasing }));
      expect(
        schema.decode({
          FOO: "this is foo",
          BAR_BAZ: "this is barBaz",
          SOMETHING_NICE_AND_LONG: "this is something nice and long",
          NESTED__FLOOF: "this is nested floof",
          NESTED__DOUBLY__SUPER_NESTED: "this is soooo nested",
          __: "should tolerate zero-phrase (becomes disregarded)",
          _: "should tolerate zero-word in phrase (becomes disregarded)",
          ___: "(becomes disregarded)",
          ____: "(becomes disregarded)",
        })
      ).toEqual(right(decodedValue));
    })
  );
});
