export type WordCasingStyle =
  | "lowerCamelCase"
  | "upperCamelCase"
  | "lowerSnakeCase"
  | "upperSnakeCase"
  | "lowerKebabCase"
  | "upperKebabCase";
