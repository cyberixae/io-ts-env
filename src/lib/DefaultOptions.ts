import { EnvDecoderOptions } from "./EnvDecoderOptions";

export const DefaultOptions: Required<EnvDecoderOptions> = {
  objectSeparator: "__",
  wordSeparator: "_",
  wordCasing: "lowerCamelCase",
};
